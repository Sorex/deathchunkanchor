package com.cyprias.DeathChunkAnchor.listeners;

import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.cyprias.DeathChunkAnchor.Logger;
import com.cyprias.DeathChunkAnchor.Plugin;
import com.cyprias.DeathChunkAnchor.configuration.Config;

public class WorldListener  implements Listener {
	static public void unregisterEvents(JavaPlugin instance) {
		ChunkUnloadEvent.getHandlerList().unregister(instance);
	}
	
	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onChunkUnload(ChunkUnloadEvent event) {
		// Don't do anything if another plugin has canceled the event.
		if (event.isCancelled())
			return;
		
		Chunk c = event.getChunk();
		if (!EntityListener.deathChunks.containsKey(c))
			return;

		//Logger.debug("Chunk has had a death.");
		
		Long deathTime = EntityListener.deathChunks.get(c);

		if ((Plugin.getUnixTime() - deathTime) > (Config.getInt("properties.loaded-duration"))){
			Logger.debug(c.getX() + " " + c.getZ() +  ": Death was too long ago, removing.");
			EntityListener.deathChunks.remove(c);
			return;
		}
		
		
		
		Logger.debug(c.getX() + " " + c.getZ() +  ": Keeping chunk loaded...");
		event.setCancelled(true);
	}
	
	
	
}
