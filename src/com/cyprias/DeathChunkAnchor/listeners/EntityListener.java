package com.cyprias.DeathChunkAnchor.listeners;

import java.util.HashMap;

import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.cyprias.DeathChunkAnchor.Logger;
import com.cyprias.DeathChunkAnchor.Perm;
import com.cyprias.DeathChunkAnchor.Plugin;
import com.cyprias.DeathChunkAnchor.configuration.Config;

public class EntityListener  implements Listener {
	static public void unregisterEvents(JavaPlugin instance) {
		PlayerDeathEvent.getHandlerList().unregister(instance);
	}
	
	/*
	class deathInfo {
		private Player player;
		private long unixTime;
		public deathInfo(Player p, long unixTime) {
			this.player = p;
			this.unixTime = unixTime;
		}
		public Player getPlayer(){
			return this.player;
		}
		public long getUnixTime(){
			return this.unixTime;
		}
	}
	*/
	
	public static HashMap<Chunk, Long> deathChunks = new HashMap<Chunk, Long>();
	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player p = (Player) event.getEntity();
		if (!Plugin.hasPermission(p, Perm.ANCHOR))
			return;

		Chunk c = p.getLocation().getChunk();

		if (Config.getBoolean("properties.anchor-surrounding-chunks")){
			int cX = c.getX();
			int cZ = c.getZ();
			
			int r=Config.getInt("properties.surrounding-radius");
			
			for (int cXi = (cX-r); cXi <= (cX+r); cXi++)
				for (int cZi = (cZ-r); cZi <= (cZ+r); cZi++)
					anchorChunk(c.getWorld().getChunkAt(cXi, cZi), p);

		}else
			anchorChunk(c, p);
		
		
	}
	
	private void anchorChunk(Chunk c, Player p){
		Logger.debug("Anchoring " + c.getX() + " " + c.getZ());
		deathChunks.put(c, Plugin.getUnixTime());
	}
	
}
